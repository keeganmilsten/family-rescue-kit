#/bin/bash

function Install {
  RUID=$(who | awk 'FNR == 1 {print $1}')
  if [[ ! -d /usr/share/rescue-kit ]]; then
      sudo mkdir /usr/share/rescue-kit
  fi
  sudo chown -R $RUID:$RUID /usr/share/rescue-kit
  sudo cp -f ./family.sh /usr/share/rescue-kit/
  sudo chmod 777 /usr/share/rescue-kit/family.sh
  sudo chown $RUID:$RUID /usr/share/rescue-kit/family.sh
  sudo cp -f ./family.desktop /usr/share/applications/
  sudo chmod 777 /usr/share/applications/family.desktop
  sudo chown $RUID:$RUID /usr/share/applications/family.desktop
}

function SearchUpdates {
  if [[ ! -f /usr/share/rescue-kit/updates/current_version.txt ]]; then
      mkdir /usr/share/rescue-kit/updates
      echo "0" >/usr/share/rescue-kit/updates/current_version.txt
  fi
  wget -q --spider http://google.com
  if [ $? -eq 0 ]; then
    RUID=$(who | awk 'FNR == 1 {print $1}')
    latest_version=$(git ls-remote --tags https://gitlab.com/keeganmilsten/family-rescue-kit.git | grep -v "{" | awk '{print $2}' | tail -1)
    version_number=${latest_version##*/}
    version_digit=${version_number%.*}
    git clone https://gitlab.com/keeganmilsten/family-rescue-kit.git --branch $version_number --depth 1 /home/$RUID/rescue-kit
    cd /home/$RUID/rescue-kit
    comments=$(git tag -l -n99 $version_number | tr -d $version_number | sed 's/^ *//' | sed 's/^/\  \  /')

    if (( "$(head -n 1 /usr/share/rescue-kit/updates/current_version.txt)" != "$version_digit" )); then
        install_updates=$(yad --width=500 --title="Keegan's Rescue Kit" --on-top --center --text "<b><big><big>   An update is available. Would you like to install it?</big></big></b>\n\n This update includes the following features: \n $comments" --button=gtk-no:1 --button=gtk-yes:0)
        ret=$?
        if [[ $ret -eq 0 ]]; then
            cd /home/$RUID/rescue-kit/
            echo $version_digit >/usr/share/rescue-kit/updates/current_version.txt
            bash ./family.sh Install
        fi
    else
        yad --width=500 --title="Keegan's Rescue Kit" --text-align=center --on-top --center --text "<b><big><big>No new Keegan updates Found</big></big></b>"
    fi
    cd ${0%/*}
    rm -rf /home/$RUID/rescue-kit
  fi
}

export -f Install SearchUpdates
"$@"
